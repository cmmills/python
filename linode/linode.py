#!/usr/bin/env python3
"""
Date: August 9, 2019
Author: Clarence Mills
Link: https://www.linode.com/docs/platform/api/using-the-linode-cli/
Dependencies: linode-cli application installed, configured user linode
              configuration file ~/.linode-cli minimal configuration should
              contain authorized token
"""

import subprocess as sp
import time
import os
import sys
import shutil
import csv

# Variables
version = "0.11"
root_user = "root"                      # Config_runner uses to log ino server
stack_script_id = "566654"              # Configured within my Linode account
host_file = "/home/cmills/.ssh/known_hosts"
linode_work_dir = "/var/tmp/linode"

# Define ansible related items
require_file = "ansible_requirements.yml"
inventory = "inventory"
playbook = "checks.yml"
ansible_config = "ansible.cfg"
ansible_config_file = os.path.join(linode_work_dir, ansible_config)
playbook_file = os.path.join(linode_work_dir, playbook)
require_file_path = os.path.join(linode_work_dir, require_file)
roles_path = os.path.join(linode_work_dir, "roles")
inventory_file = os.path.join(linode_work_dir, inventory)


def clean_up():
    # Clean up, remove old generated files
    if os.path.isfile(inventory_file):
        print("removing " + inventory_file)
        os.remove(inventory_file)
    if os.path.isfile(playbook_file):
        print("removing " + playbook_file)
        os.remove(playbook_file)
    if os.path.isfile(require_file_path):
        print("removing " + require_file_path)
        os.remove(require_file_path)
    if os.path.isfile(ansible_config_file):
        print("removing " + ansible_config_file)
        os.remove(ansible_config_file)
    if os.path.isdir(roles_path):
        print("removing " + roles_path)
        shutil.rmtree(roles_path)


def create_ansible_config():
    ansible_content = """[defaults]
inventory 		        = /var/tmp/linode/inventory
remote_tmp     		    = /var/tmp/
local_tmp      		    = ~/.ansible/tmp
transport      		    = smart
retry_files_enabled     = false
log_path 		        = ~/.ansible/ansible-logs.txt
local_tmp 		        = ~/.ansible/tmp
host_key_checking 	    = False
validate_certs		    = False

[ssh_connection]
control_path 		    = ~/.ansible/tmp/ansible-ssh-%%h-%%p-%%r
pipelining 		        = True
record_host_keys 	    = False
scp_if_ssh 		        = True
transfer_method 	    = smart
"""
    if os.path.exists(ansible_config_file):
        print("Found existing ansible config file " + ansible_config_file +  " recreating ...")
        os.remove(ansible_config_file)
        file = open(ansible_config_file, 'w+')
        file.writelines(ansible_content)
        file.close()
        return ansible_config_file
    else:
        print("Creating ansible config file " + ansible_config_file)
        file = open(ansible_config_file, 'w+')
        file.writelines(ansible_content)
        file.close()
        return ansible_config_file


def create_playbook():
    playbook_content = """---
- hosts: "{{ server_name }}"
  become: True
  gather_facts: False
  pre_tasks:
    - name: Wait for port 22 to become available
      wait_for_connection:
        delay: 30
    - name: Run setup   # Gather facts that playbooks depend on
      setup:
  roles:                # Include roles to configure based user and apps
    - common
    - system_security
"""
    if os.path.exists(playbook_file):
        print("Found existing playbook, " + playbook_file + " recreating ...")
        os.remove(playbook_file)
        file = open(playbook_file, 'w+')
        file.writelines(playbook_content)
        file.close()
    else:
        # os.mkdir(linode_work_dir)
        print("Creating playbook " + playbook_file)
        file = open(playbook_file, 'w+')
        file.writelines(playbook_content)
        file.close()


def create_require_file():
    require_file_content = """---
- name: common
  scm: git
  src: git+https://cmmills@bitbucket.org/opensitesolutions/common.git
  version: master

- name: system_security
  scm: git
  src: git+https://gitlab.com/cmmills/system_security.git
  version: master

- name: priv_users
  scm: git
  src: git+https://cmmills@bitbucket.org/opensitesolutions/priv_users.git
  version: master
"""
    if os.path.exists(require_file_path):
        print("Found existing requirements file " + require_file_path + " recreating ...")
        os.remove(require_file_path)
        file = open(require_file_path, 'w+')
        file.writelines(require_file_content)
        file.close()
    else:
        print("Creating requirements file " + require_file_path)
        file = open(require_file_path, 'w+')
        file.writelines(require_file_content)
        file.close()


def create_configs():
    create_playbook()
    create_require_file()
    create_ansible_config()


def show_events():
    header()
    print("Linode events:")
    sp.run(['linode-cli', 'events', 'list',
            '--format', 'status,id,username,action,seen,read', '--page', '1'],
           stderr=sp.PIPE
           )


def header():
    retrieved_acct_info = sp.run(['linode-cli', '--text', '--no-header',
                                  '--delimiter', ',', 'account', 'view'],
                                 stdout=sp.PIPE,
                                 stderr=sp.PIPE).stdout.decode('utf-8')
    acct = retrieved_acct_info.split(",")

    # Assign values
    first_name = acct[0]
    last_name = acct[1]
    email = acct[2]
    account_balance = acct[3]
    monthly_balance = acct[4]

    sp.run('clear')
    print("Linode Control Dashboard")
    print("------------------------")
    print("User name: " + first_name, last_name)
    print("Email address: " + email)
    print("Account balance: " + account_balance)
    print("Monthly spend: " + monthly_balance)


def get_transfer_amount():
    transfer_info = sp.run(['linode-cli', '--text', '--no-header',
                            '--delimiter', ',', 'account', 'transfer'],
                           stdout=sp.PIPE,
                           stderr=sp.PIPE).stdout.decode('utf-8')
    net_info = transfer_info.split(",")
    bandwidth_quota = net_info[0]
    used_bandwidth = net_info[1]
    billable = net_info[2]
    print("Assigned bandwidth: " + bandwidth_quota)
    print("Used bandwidth: " + used_bandwidth)
    print("Billable bandwidth: " + billable)


def account():
    header()
    get_transfer_amount()
    list_lin()


def config_runner():
    create_configs()

    print()
    print("Starting configuration process")
    print("------------------------------")
    print()
    if os.path.isfile(require_file):
        print("Downloading Ansible roles ....")
        sp.run(['ansible-galaxy', 'install', '-f', '-r', require_file_path, '-p',
                roles_path])

        if os.path.isfile(playbook_file):
            print("Executing playbook %s" % playbook_file)
            # time.sleep(2)
            sp.run(['ansible-playbook', '-i', inventory_file, playbook_file, '-e',
                    'server_name=all', '-vvvv'])
        else:
            print("Playbook " + playbook_file + "does not exist")
            create_playbook()
            if os.path.isfile(playbook_file):
                print("Executing playbook %s" % playbook_file)
                sp.run(['ansible-playbook', '-i', inventory_file, playbook, '-e',
                        'server_name=all', '-vvvv'])
    else:
        create_require_file()
        print("Downloading Ansible roles ....")
        sp.run(['ansible-galaxy', 'install', '-f', '-r', require_file_path, '-p',
                roles_path])
        if os.path.isfile(playbook_file):
            print("Executing playbook %s" % playbook_file)
            sp.run(['ansible-playbook', '-i', inventory_file, playbook_file, '-e',
                    'server_name=all', '-vvvv'])


def genrate_password():
    # Generate a random password don't use -base64 creates unwanted characters
    global lin_root_pw
    lin_root_pw = str.strip(sp.run(['openssl', 'rand', '-hex', '24'],
                                   stdout=sp.PIPE, stderr=sp.PIPE)
                            .stdout.decode('utf-8'))


def list_lin():
    print("""Linode information:""")
    sp.run(['linode-cli', 'linodes', 'list', '--format',
            'id,status,ipv4,updated,day,hypervisor,type\
             ,region,label,network_out,network_in'
            ], stderr=sp.PIPE)


def list_type():
    sp.run(['linode-cli', 'linodes', 'types'], stderr=sp.PIPE)
    global lin_type
    lin_type = input("Enter type of linode example g6-nanode-1: ")
    return lin_type


def list_region():
    sp.run(['linode-cli', 'regions', 'list'], stderr=sp.PIPE)
    global lin_reg
    lin_reg = input("Enter region example ca-central: ")
    return lin_reg


def list_image():
    sp.run(['linode-cli', 'images', 'list', '--format', 'id,label'],
           stderr=sp.PIPE)
    global lin_image
    lin_image = input("Enter image example linode/ubuntu18.04: ")


def get_server_label():
    # Server label
    global srv_label
    srv_label = input("Enter server label: ")


def get_ip(label):
    srv_label = label
    global lin_ip

    # Get newly created server IP address
    print("Fetching newly created IP for %s" % srv_label)
    lin_ip = sp.run(['linode-cli', '--text', '--no-header', '--format',
                    'ipv4,', 'linodes', 'list', '--label', srv_label],
                    stdout=sp.PIPE, stderr=sp.PIPE) .stdout.decode('utf-8')
    # print(lin_ip.strip())

    # Remove any traces of IP address present within known_hosts file
    print("Checking if vendor IP " + lin_ip +
          "is present within known host file and removing")
    sp.run(['ssh-keygen', '-f', host_file, '-R', lin_ip.strip()],
           stdout=sp.PIPE, stderr=sp.PIPE)


def create_ans_inv():
    # Create ansible inventory file
    print("Creating ansible inventory file " + ansible_config_file)
    print()
    with open(inventory_file, "w") as file:
        content = ([re_lin_ip.strip(), "  ansible_host=%s" % re_lin_ip.strip(),
                    " ansible_user=%s" % root_user,
                    " ansible_ssh_pass=%s" % lin_root_pw])
        file.writelines(content)


def get_state(lin_id):
    status = "not checked"
    while status != "checked":
        lin_status = str.strip(sp.run(['linode-cli', '--text', '--no-header',
                                       'linodes', 'list', '--format', 'status',
                                       '--id', lin_id],
                                      stdout=sp.PIPE, stderr=sp.PIPE)
                               .stdout.decode('utf-8'))
        sp.run(['clear'])
        print("Getting server state")
        print("--------------------------")
        print()
        print("Server state: %s" % lin_status)
        time.sleep(1)
        if lin_status == "running":
            status = "checked"
        else:
            continue


def rebuild_state(lin_id):
    status = "not checked"
    while status != "checked":
        lin_status = str.strip(sp.run(['linode-cli', '--text', '--no-header',
                                      'linodes', 'list', '--format',
                                       'status', '--id', lin_id],
                                      stdout=sp.PIPE, stderr=sp.PIPE)
                               .stdout.decode('utf-8'))
        sp.run(['clear'])
        print("Rebuilding server " + lin_id)
        print("-------------------------------")
        print()
        print("Waiting for rebuilding process to start: ")
        print("Server rebuild state: %s" % lin_status)
        time.sleep(1)
        if lin_status == "rebuilding":
            status = "checked"
            get_state(lin_id)
        else:
            continue


def restart_lin():
    header()
    list_lin()
    lin_id = input("Choose linode Id from the list: ")
    sp.run(['linode-cli', 'linodes', 'reboot', lin_id], stdout=sp.PIPE,
           stderr=sp.PIPE)
    print("Restarting Server %s" % lin_id)
    time.sleep(2)
    get_state(lin_id)


def del_lin():
    """ Delete a linode server based on linode id passed in """
    header()
    list_lin()
    print("Delete linode server")
    lin_id = input("Enter linode ID: ")
    sp.run(['linode-cli', 'linodes', 'delete', lin_id], stderr=sp.PIPE)
    account()
    clean_up()


def rebuild_lin():
    header()
    genrate_password()
    list_lin()
    print("Rebuild Linode:")
    lin_id = input("Choose linode Id from the list: ")
    list_image()
    if lin_image == "linode/ubuntu18.04":
        # Rebuild linode server
        sp.run(['linode-cli', 'linodes', 'rebuild', '--root_pass', lin_root_pw,
                '--image', lin_image, lin_id, '--stackscript_id',
                stack_script_id], stdout=sp.PIPE, stderr=sp.PIPE)
        # print("root password is: %s" % (lin_root_pw))
        global re_lin_ip
        rebuild_state(lin_id)
    else:
        # Rebuild linode server
        sp.run(['linode-cli', 'linodes', 'rebuild', '--root_pass', lin_root_pw,
                '--image', lin_image, lin_id], stdout=sp.PIPE, stderr=sp.PIPE)

        # print("root password is: %s" % (lin_root_pw))
        global re_lin_ip
        rebuild_state(lin_id)

    # Get IP address for specific linode being built
    re_lin_ip = sp.run(['linode-cli', '--text', '--no-header',
                        '--format', 'ipv4', 'linodes', 'list',
                        '--id', lin_id], stdout=sp.PIPE, stderr=sp.PIPE) \
        .stdout.decode('utf-8')

    # Create ansible inventory file
    create_ans_inv()

    # Remove any traces of IP address present within known_hosts file
    print("Checking if vendor IP is present within known hosts file and removing")
    sp.run(['ssh-keygen', '-f', host_file, '-R', re_lin_ip.strip()])

    # Configure linode server, add user and final configuration
    config_runner()
    account()


def create_lin():
    header()
    print("""Build a brand new Linode server""")
    genrate_password()
    get_server_label()
    list_region()
    list_type()
    list_image()
    lin_tag = input("Enter tag name: ")

    # Build server with options from above
    if lin_image == "linode/ubuntu18.04":
        sp.run(['linode-cli', 'linodes', 'create',
                '--root_pass', lin_root_pw,
                '--label', srv_label, '--region', lin_reg,
                '--type', lin_type, '--image', lin_image,
                '--tags', lin_tag, '--stackscript_id', stack_script_id],
               stdout=sp.PIPE, stderr=sp.PIPE)
        """
        Get new generated linode ID to be able to ge the state as it's
        being built
        """
        new_lin_id = str.strip(sp.run(['linode-cli', '--text', '--no-header',
                                       'linodes', 'list', '--label', srv_label,
                                       '--format', 'id'], stdout=sp.PIPE,
                                      stderr=sp.PIPE)
                                 .stdout.decode('utf-8'))
        get_state(new_lin_id)
    else:
        # Install linode without stackscript
        sp.run(['linode-cli', 'linodes', 'create',
                '--root_pass', lin_root_pw,
                '--label', srv_label, '--region', lin_reg,
                '--type', lin_type, '--image', lin_image,
                '--tags', lin_tag], stdout=sp.PIPE, stderr=sp.PIPE)
        """
        Get new generated linode ID to be able to ge the state as it's
        being built
        """
        new_lin_id = str.strip(sp.run(['linode-cli', '--text', '--no-header',
                                       'linodes', 'list', '--label', srv_label,
                                       '--format', 'id'], stdout=sp.PIPE,
                                      stderr=sp.PIPE)
                                 .stdout.decode('utf-8'))
        get_state(new_lin_id)

    # Get IP address from newly created server
    get_ip(srv_label)

    # Create ansible inventory file
    with open(inventory_file, "w") as file:
        content = ([srv_label, "  ansible_host=%s" % lin_ip.strip(),
                   " ansible_user=%s" % root_user,
                    " ansible_ssh_pass=%s" % lin_root_pw])
        file.writelines(content)

    # Configure linode server, add user and final configuration
    config_runner()
    account()


def build_from_file():
    print("""Building a brand new Linode server""")
    genrate_password()
    user_file = input("Enter path to the file name: ")
    with open(user_file, 'r') as csv_file:
        csv_read_file = csv.DictReader(csv_file)
        for line in csv_read_file:
            srv_label = line['label']
            lin_reg = line['region']
            lin_type = line['type']
            lin_image = line['image']
            lin_tag = line['tag name']
    if lin_image == "linode/ubuntu18.04":
        sp.run(['linode-cli', 'linodes', 'create',
                '--root_pass', lin_root_pw,
                '--label', srv_label, '--region', lin_reg,
                '--type', lin_type, '--image', lin_image,
                '--tags', lin_tag,
                '--stackscript_id', stack_script_id], stdout=sp.PIPE, stderr=sp.PIPE)
    else:
                sp.run(['linode-cli', 'linodes', 'create',
                '--root_pass', lin_root_pw,
                '--label', srv_label, '--region', lin_reg,
                '--type', lin_type, '--image', lin_image,
                '--tags', lin_tag], stdout=sp.PIPE, stderr=sp.PIPE)

    """
    Get new generated linode ID to be able to ge the state as it's
    being built
    """
    new_lin_id = str.strip(sp.run(['linode-cli', '--text', '--no-header',
                                   'linodes', 'list', '--label', srv_label,
                                   '--format', 'id'], stdout=sp.PIPE,
                                  stderr=sp.PIPE)
                           .stdout.decode('utf-8'))
    get_state(new_lin_id)
    
    # Get IP address from newly created server
    get_ip(srv_label)

    # Create ansible inventory file
    with open(inventory_file, "w") as file:
        content = ([srv_label, "  ansible_host=%s" % lin_ip.strip(),
                   " ansible_user=%s" % root_user,
                    " ansible_ssh_pass=%s" % lin_root_pw])
        file.writelines(content)

    # Configure linode server, add user and final configuration
    config_runner()
    account()


def lin_help():
    global help
    help = """

The available options for execution are printed below

OPTIONS:
    -a, account    list account information
    -c, config     Create config files
    -e, events     show linode events
    -l, list       list linodes within your account
    -b, build      Build a new linode server
    -f, file       Build a linode from options within a csv file
    -r, rebuild    Rebuild a Linode server
    -d, delete     Delete a Linode server
    -rc, reconfig  Reapply base pre build configuration, this is
                   usually ansible run commands.
    -rs, restart   Restart a linode server.
    -v, version    Print version
    -C, clean      Remove old configuration related files
    -h, help       Print this help message
                                     """
    print("Usage: " + sys.argv[0] + " [ OPTIONS ]" + help)


if int(len(sys.argv)) == 1:
    lin_help()
elif (sys.argv[1] == "account" or sys.argv[1] == "-a"):
    account()
elif sys.argv[1] == "config" or sys.argv[1] == "-c":
    create_configs()
elif (sys.argv[1] == "list" or sys.argv[1] == "-l"):
    list_lin()
elif (sys.argv[1] == "rebuild" or sys.argv[1] == "-r"):
    rebuild_lin()
elif (sys.argv[1] == "build" or sys.argv[1] == "-b"):
    create_lin()
elif (sys.argv[1] == "delete" or sys.argv[1] == "-d"):
    del_lin()
elif sys.argv[1] == "file" or sys.argv[1] == "-f":
    build_from_file()
elif (sys.argv[1] == "reconfig" or sys.argv[1] == "-rc"):
    config_runner()
elif (sys.argv[1] == 'restart' or sys.argv[1] == "-rs"):
    restart_lin()
elif (sys.argv[1] == "version" or sys.argv[1] == "-v"):
    print(sys.argv[0] + " " + version)
elif (sys.argv[1] == "events" or sys.argv[1] == "-e"):
    show_events()
elif (sys.argv[1] == "clean" or sys.argv[1] == "-C"):
    clean_up()
elif (sys.argv[1] == "help" or sys.argv[1] == '-h'):
    lin_help()
elif sys.argv[1] == "conf":
    print("Option not implemented as yet")
else:
    lin_help()
