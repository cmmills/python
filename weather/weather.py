#! /usr/bin/env python3

# from distutils.command.config import config
# from email import parser
import json
import requests
import argparse
from configparser import ConfigParser
import os

def get_api_key():
    # Get the api key from your configuration file
    user = os.path.expanduser("~")                  # Get the user's home directory
    if os.path.exists(user + "/.settings.ini"):
        config = ConfigParser()
        config.read(user + "/.settings.ini")
        return config["openweather"]["api_key"]
    else:
        print("No configuration file \"settings.ini\" found")
        exit(1)

parser = argparse.ArgumentParser(description="Print out the weather for a City passed in from the command line")
parser.add_argument("city", type=str, help="Enter the city name")
parsed_args = parser.parse_args()

owm_key = get_api_key()
city = parsed_args.city
units = "metric"
url = "http://api.openweathermap.org/data/2.5/weather?q=" + \
    city + "&units=" + units + "&appid=" + owm_key

r = requests.get(url)
data = r.json()
json_data = json.dumps(data, indent=4)
print(json_data)
print(f"City of {data['name']}: temperature {str(data['main']['temp'])} feel like {str(data['main']['feels_like'])}  Humidity {str(data['main']['humidity'])} with {data['weather'][0]['description']}  ")
