# Weather application

This application is to be run from a unix command line

## Create settings.ini file

This file should contain your API key from openweathermap, you can sign up for a free account from [openweathermap](https://openweathermap.org)

```bash
[openweather]
api_key = "<your_api_key>"
```