#! /usr/bin/env python3


import os
import shutil
import time
import subprocess
import argparse
import socket


def clear():
    os.system('clear')

def install_dev_tools():
    # Install Development Tools
    dev_list = [ "sudo apt-get -y update",
        "sudo apt-get -y install tree",
        "sudo apt-get -y install golang",
        "sudo apt-get -y install vlc",
        "sudo apt-get -y install tmux",
        "sudo apt-get -y install timeshift",
        "sudo apt-get -y install htop",
        "sudo apt-get -y install btop",
        "sudo apt-get -y install ncdu",
        "sudo apt-get -y install lsb-release",
        "sudo apt-get -y install curl",
        "sudo apt-get -y install git",
        "sudo apt-get -y install tldr",
        "sudo apt-get -y install neofetch",
        "sudo apt-get -y install nala",
    ]
    # 
    print("Installing development tools")
    time.sleep(2)
    for item in dev_list:
        os.system(item)
    print("Development tools installed")


def setup_git_config():
    clear()
    print("Setting up Git configuratin file")
    time.sleep(2)
    # 
    # Setup gitconfig
    os.system('git config --global user.name "Clarence Mills"')
    os.system('git config --global user.email "cmmills@protonmail.com"')
    os.system('git config --global core.editor "code --wait"')
    os.system('git config --global init.defaultBranch main')
    os.system('git config --global core.excludesfile ~/.gitignore_global')
    os.system('git config --global help.autocorrect 1')
    print("Git configuration file setup complete")

def setup_ssh():
    clear()
    print("Setting up SSH")
    time.sleep(2)
    # Setup SSH
    username = os.environ['USER']
    bits = int("4096")
    keytype = "rsa"
    comment = username + "@" + socket.gethostname()
    ssh_dir = os.path.join(os.environ['HOME'], ".ssh")
    os.system('ssh-keygen -t keytype -C comment -P "" -f  ssh_dir')

def install_cheat_sh():
    clear()
    print("setting up cheat.sh")
    time.sleep(2)
    PATH_DIR = os.path.expanduser("~")
    bin_dir = os.path.join(PATH_DIR, "bin")
    
    if not os.path.exists(bin_dir):
        os.makedirs(bin_dir)
    os.system('curl -s https://cht.sh/:cht.sh > ~/bin/cht.sh')
    os.system('chmod +x ~/bin/cht.sh')
    os.system("alias cheat='~/bin/cht.sh' >> ~/.bashrc")
    print("E.g. cheat python os")

def create_sudo_file():
    clear()
    print("Setting up sudo for user " + os.environ['USER'])
    time.sleep(2)
    file = os.environ['HOME'] + "/" + os.environ['USER']
    print("Creating file " + file + " in " + os.environ['HOME'])
    with open(file, 'w') as f:
        print(os.environ['USER'] + " ALL=(ALL) NOPASSWD:ALL", file=f)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--all", help="Install all development tools", action="store_true")
    parser.add_argument("-g", "--git", help="Setup git configuration file", action="store_true")
    parser.add_argument("-s", "--ssh", help="Setup SSH", action="store_true")
    parser.add_argument("-c", "--cheat", help="Install cheat.sh", action="store_true")
    parser.add_argument("-d", "--sudo", help="Create sudo file", action="store_true")
    args = parser.parse_args()

    if args.all:
        install_dev_tools()
        setup_git_config()
        setup_ssh()
        install_cheat_sh()
        create_sudo_file()
    elif args.git:
        setup_git_config()
    elif args.ssh:
        setup_ssh()
    elif args.cheat:
        install_cheat_sh()
    elif args.sudo:
        create_sudo_file()
    else:
       parser.print_help()