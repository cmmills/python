# Python

## Pyinstaller

```pyinstaller --onefile proxmox.py --name proxmox```
This will build a standalone binary of the file ```proxmox.py``` script

## Pipreqs

```pipreqs < path to the python script>```
- Pipreqs will examine the python script modules and will create a requirements file containning modules required by the script.
- You can then run the following command ```pip install -r requirements```

## Code2flow

```code2flow . --target-function=create_host --downstream-depth=5 --upstream-depth=4```
- [Code2flow](https://github.com/scottrogowski/code2flow)
- install ```pip3 install code2flow```
- approximates the structure of projects in dynamic languages 