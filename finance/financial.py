#!/usr/bin/env python3
"""
notify-py - https://pypi.org/project/notify-py/
Dependencies: 
- apt-get install -y python3-loguru
- apt install python3-jeepney

"""

import mysql.connector
import getpass
import datetime
from datetime import timedelta
import argparse
from notifypy import Notify
from dotenv import load_dotenv, dotenv_values
import os


def notify(title, message):
    notification = Notify()
    notification.title = title
    notification.message = message
    notification.urgency = 'critical'
    notification.send()

def connect_db():
    load_dotenv()
    mysql_user = os.getenv('MYSQL_USER')
    mysql_password = os.getenv('MYSQL_PASSWORD')

    # Connect to the database
    db = mysql.connector.connect(
        host=os.getenv('MYSQL_HOST'),
        user=os.getenv('MYSQL_USER'),
        passwd=os.getenv('MYSQL_PASSWORD'),
    )
    return db

def get_bills_due():
    date = datetime.datetime.now()
    day = date.strftime("%d")
    today = date.today()

    db = connect_db()
    cursor = db.cursor() 
    cursor.execute('select date,company,currency,bill_usage,dollar_value,notes,bank from finance.bills where occurence != "yearly"')
    results = cursor.fetchall()

    yesterday = today - timedelta(days=1)
    seven_days_ahead = date + datetime.timedelta(days=7)
    ten_days_ahead = date + datetime.timedelta(days=10)
    two_weeks_ahead = date + datetime.timedelta(days=14)

    print("Today's date is: ", today.strftime("%d"))
    for row in results:
        if row[0] == yesterday.strftime("%d"):
            print("Bills due yesterday were: " + row[1],row[4],row[0])
            title = "Bills due yesterday"
            message = row[1],row[4],row[0]
            notify(title, message)

        if row[0] == day:
            print("Bills due today are: " + row[1],row[4],row[0])
            title = "Bills due today"
            message = row[1],row[4],row[0]
            notify(title, message)

        if row[0] == seven_days_ahead.strftime("%d"):
            print("Bills due in 7 dayas are: " + row[1],row[4],row[0])
            title = "Bills due in 7 days"
            message = row[1],row[4],row[0]
            notify(title, message)

        if row[0] == ten_days_ahead.strftime("%d"):
            print("Bills due in 10 days are: " + row[1],row[4],row[0])
            title = "Bills due in 10 days"
            message = row[1],row[4],row[0]
            notify(title, message)

        if row[0] == two_weeks_ahead.strftime("%d"):
            print("Bills due in 14 days are: " + row[1],row[4])
            message = row[1],row[4]
            title = "Bills due in 14 days"
            notify(title, message)
    db.close()

def add_db_entry():
    # Add new entry to bills database
    db = connect_db()
    cursor = db.cursor()

    cursor.execute('insert into finance.bills (bank,date,company,currency,bill_usage,dollar_value,occurence,status,notes) values ("visa","16","Medium corporation","CA","personal","70.81","yearly","actve","March 16, 2024")')
    db.commit()
    db.close()

def main():
    parser = argparse.ArgumentParser(description='Get bills that are due from databases.')
    parser.add_argument('--add', dest='add', action='store_true')
    parser.add_argument('--bills', dest='bills', action='store_true', default=True)
    args = parser.parse_args()

    if args.add:
        add_db_entry()
    elif args.bills:
        get_bills_due()
    else:
        parser.print_help()

if __name__ == "__main__":
    main()
        
