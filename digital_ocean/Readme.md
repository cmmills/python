# Automating digital ocean

## Droplet objects
- [Objects](https://slugs.do-api.dev)


## Documentation
- Python API - written by a third pary [https://github.com/koalalorenzo](https://github.com/koalalorenzo/python-digitalocean)
- Digitla Ocean official Python client [https://github.com/digitalocean](https://github.com/digitalocean/pydo)
    - [Readthedocs](https://digitalocean-api-python-client.readthedocs.io/en/latest/index.html)