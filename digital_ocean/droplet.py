#! /usr/bin/env python3

"""
python api: https://github.com/koalalorenzo/python-digitalocean
Manage droplets witin Digital Ocean
"""
import digitalocean
from dotenv import load_dotenv
import os
import argparse

def create_droplet(droplet_name, region, image, slug_size):
    load_dotenv()
    token = os.getenv('DO_TOKEN')
    manager = digitalocean.Manager(token=token)
    keys = manager.get_all_sshkeys()

    droplet = digitalocean.Droplet(token=token,
                               name=droplet_name,
                               region=region,           # obtain region all options from https://slugs.do-api.dev
                               image=image,             # Operating system: Ubuntu 20.04 x64
                               size_slug=slug_size,     # memory and cpu: 1GB RAM, 1 vCPU basically slug size
                               ssh_keys=keys,           # ssh keys                               
                               backups=False)
    droplet.create()
    get_droplet_status(droplet)


def get_droplet_status(droplet):
    actions = droplet.get_actions()
    for action in actions:
        action.load()
        # Once it shows "completed", droplet is up and running
        print(action.status)

def delete_droplet(droplet_name): # Does not work
    load_dotenv()
    token = os.getenv('DO_TOKEN')
    droplet = digitalocean.Droplet(token=token,
                               name=droplet_name)
    droplet.destroy()

def add_ssh_key(key_name):
    load_dotenv()
    token = os.getenv('DO_TOKEN')
    ssh_key = (os.environ['HOME'] + '/.ssh/id_rsa.pub')
    user_ssh_key = open(ssh_key).read()
    ssh_key = digitalocean.SSHKey(token=token,
                                  name=key_name,
                                  public_key=user_ssh_key)
    ssh_key.create()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Manage droplets within Digital Ocean')
    parser.add_argument('-c', '--create', help='Create a droplet', nargs=4)
    parser.add_argument('-d', '--delete', help='Delete a droplet', nargs=1)
    parser.add_argument('-k', '--key', help='Add ssh key', nargs=1)
    args = parser.parse_args()

    if args.create:
            droplet_name = args.create[0]
            region = args.create[1]
            image = args.create[2]
            slug_size = args.create[3]
            create_droplet(droplet_name, region, image, slug_size)
    elif args.key:
            key_name = args.key[0]        
            add_ssh_key(key_name)
    elif args.delete:
            droplet_name = args.delete[0]
            delete_droplet(droplet_name)
    else:
         parser.print_help()
