#!/usr/bin/env python3

"""
Date: September: 2, 2019
Author: Clarence Mills
Scope: Manage Vultr through API
"""

import os
import json
import requests
import subprocess as sp
import sys
import time

token_file = "/home/cmills/.vultr.ini"
vultr_tmp_dir = "/var/tmp/vultr"
stored_token_file = "vultr_token.ini"


def header():
    # sp.run(['clear'])
    print("Vultr Dashboard")
    print("---------------")
    print()


def get_os():   # Get a list of Operating systems, no API key required
    url = 'https://api.vultr.com/v1/os/list'

    r = requests.get(url)
    os = r.json()      # Convet to dictionary

    os_list_str = json.dumps(os, indent=2, sort_keys=True)
    print(os_list_str)


def get_regions():  # Get all available regions, No API key required
    url = 'https://api.vultr.com/v1/regions/list'

    r = requests.get(url)
    regions_json = r.json()

    regions_str = json.dumps(regions_json, indent=2)
    print(regions_str)


def get_plans():   # Gets a single plan, No API key required
    url = 'https://api.vultr.com/v1/plans/list?type=vc2'

    r = requests.get(url)
    plans_json = r.json()

    plans_str = json.dumps(plans_json, indent=2)
    print(plans_str)


def get_token():
    header()
    fullpath = os.path.join(vultr_tmp_dir, stored_token_file)
    global key
    if os.path.exists(fullpath):
        with open(fullpath, "r") as file:
            vultr_token = file.read(40)
            key = "API-KEY:" + vultr_token
    else:
        if os.path.exists(vultr_tmp_dir):
            vultr_token = input("Enter token: ")
            key = "API-KEY:" + vultr_token

            """ store key for future use """
            with open(fullpath, "w+") as f:
                f.write(vultr_token)
        else:
            os.makedirs(vultr_tmp_dir)
            vultr_token = input("Enter token: ")
            key = "API-KEY:" + vultr_token

            """ store key for future use """
            with open(fullpath, "w+") as f:
                f.write(vultr_token)



def account():
    get_token()

    """ Get account information """
    acct_recv = sp.run(['curl', '-H',
                        key, 'https://api.vultr.com/v1/account/info'],
                       stdout=sp.PIPE, stderr=sp.PIPE).stdout.decode('utf-8')
    data = json.loads(acct_recv)
    print("Balance: " + (data['balance']))
    print("Pending charges: " + (data['pending_charges']))
    print("last payment amount: " + (data['last_payment_amount']))
    print('Last payment date: ' + (data['last_payment_date']))


def get_state(id):
    get_token()
    vultr_subid = id
    url = "https://api.vultr.com/v1v1/server/list?SUBID=" + vultr_subid
    vultr_state = "closed"
    while vultr_state != "active":
        v_node = sp.run(['curl', '-H', url], stdout=sp.PIPE,
                        stderr=sp.PIPE).stdout.decode('utf-8')
        data = json.loads(v_node)
        vultr_state = data[vultr_subid]+'[status]'
        time.sleep(1)


def list_vultr_nodes():
    get_token()
    account()

    print()
    vultr_list = sp.run(['curl', '-H', key,
                         'https://api.vultr.com/v1/server/list'],
                        stdout=sp.PIPE,
                        stderr=sp.PIPE).stdout.decode('utf-8')
    print(type(vultr_list))

    data = json.loads(vultr_list)   # Convert string to dict

    # Prints out data in json format, indented with two spaces
    print(json.dumps(data, indent=2))  # dump to json


def destroy_vultr_srv():
    get_token()
    account()
    print()
    vultr_id = input("Enter Vultr sub-id: ")
    sp.run(['curl', '-H', key, 'https://api.vultr.com/v1/server/destroy',
            '--data', vultr_id])


def create_vultr_srv():
    get_token()
    account()
    print()
    sub_id = sp.run(['curl', '-H', key,
                     'https://api.vultr.com/v1/server/create',
                     '--data', 'DCID=22', '--data',
                     'VPSPLANID=203',
                     '--data', 'OSID=127'], stdout=sp.PIPE,
                    stderr=sp.PIPE).stdout.decode('utf-8')

    sid = json.loads(sub_id)    # Convert string to DICT
    print(sid['SUBID'])         # Display value from SUBID key
    vultr_sid = sid['SUBID']
    get_state(vultr_sid)


def vultr_help():
    prg = sys.argv[0]
    vultr_help = """Available options are

OPTIONS:
    - a, account    list account information
    - b, build      Build a vultr server
    - D, destroy    Destroy vultr server
    - h, help       Print this mesage
    - l, list       Print list of vultr servers
    - o, operating systems  List all operating systems
    - p, plans      list all plans
    - r, regions    list all available regions
    - rr, regions   list regions

"""
    print("Usage: " + os.path.basename(prg) + " [OPTIONS] " + vultr_help)

if int(len(sys.argv)) == 1:
    vultr_help()
elif sys.argv[1] == "-a" or sys.argv[1] == "account":
    account()
elif sys.argv[1] == '-b' or sys.argv[1] == "build":
    create_vultr_srv()
elif sys.argv[1] == '-D' or sys.argv[1] == "delete":
    destroy_vultr_srv()
elif sys.argv[1] == "-h" or sys.argv[1] == "help":
    vultr_help()
elif sys.argv[1] == "-l" or sys.argv[1] == "list":
    list_vultr_nodes()
elif sys.argv[1] == "-o" or sys.argv[1] == "os":
    get_os()
elif sys.argv[1] == "-p" or sys.argv[1] == "plans":
    get_plans()
elif sys.argv[1] == "-r" or sys.argv[1] == "regions":
    get_regions()
else:
    vultr_help()
