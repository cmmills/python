
# Filling variables from dict obtain API response

```
vultr_subid = "28390043"

print("vultr information:")
print("-------------------")
print("Date created: " + vultr_list[vultr_subid]['date_created'])
print("location: " + vultr_list[vultr_subid]['location'])
print("vultr ID: " + vultr_list[vultr_subid]['SUBID'])
print("Power status: " + vultr_list[vultr_subid]['status'])
print("Monthly cost: " + vultr_list[vultr_subid]['cost_per_month'])
print("Pending chargers: " + vultr_list[vultr_subid]['pending_charges'])
print("IPV4 address: " + vultr_list[vultr_subid]['main_ip'])
print("Server State: " + vultr_list[vultr_subid]['server_state'])
print()
print("Virtual CPU: " + vultr_list[vultr_subid]['vcpu_count'])
print("Disk : " + vultr_list[vultr_subid]['disk'])
print("Memory: " + vultr_list[vultr_subid]['ram'])
print("Operating System: " + vultr_list[vultr_subid]['os'])
print("KVM URL: " + vultr_list[vultr_subid]['kvm_url'])
```