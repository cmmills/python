#!/usr/bin/env python3

from pytube import YouTube
from pytube.exceptions import VideoUnavailable, PytubeError
import yt_dlp
import sys
import argparse

"""
Download a video from youtube using pytube
https://pytube.io/en/latest/user/quickstart.html
"""

def on_progress(stream, chunk, bytes_remaining):
    total_size = stream.filesize
    bytes_downloaded = total_size - bytes_remaining
    percentage_of_completion = bytes_downloaded / total_size * 100
    print(f'Download progress: {percentage_of_completion:.2f}%')

def download_video(url):
    download_path = "/mnt/omv/media/music_videos/"
    yt = YouTube(url, use_oauth=False, allow_oauth_cache=True)                              # Create a YouTube object
    stream = yt.streams.filter(progressive=True, 
                    file_extension='mp4',resolution='720p')       # Get a stream with resolution of 720p and file extension of mp4
    print("Downloading video from: " + url)
    print(yt.title, yt.description, sep="\n")
    stream.first().download(download_path)                          # Download the first(highest quality video) video to the download_path

def download_music(url):
    download_path = "/mnt/omv/media/music/"
    
    ydl_opts = {
        'format': 'bestaudio/best',
        'outtmpl': f'{download_path}%(title)s.%(ext)s',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
    }
    
    try:
        with yt_dlp.YoutubeDL(ydl_opts) as ydl:
            print(f"Downloading music from: {url}")
            ydl.download([url])
            print("Download completed.")
    except Exception as e:
        print(f"Error downloading music from: {url}")
        print(str(e))

def download_movie(url, download_path="/mnt/omv/media/movies/", filename=None, resolution='720p', show_metadata=True, log_file='download_log.txt'):
    try:
        yt = YouTube(url, on_progress_callback=on_progress, use_oauth=False, allow_oauth_cache=True)  # Enable OAuth and progress callback

        if show_metadata:
            print("Title:", yt.title)
            print("Description:", yt.description)
            print("Available streams:")
            for stream in yt.streams:
                print(stream)

        # Attempt to find the specified resolution mp4 stream
        stream = yt.streams.filter(progressive=True, file_extension='mp4', resolution=resolution).first()

        if not stream:
            print(f"No {resolution} mp4 stream found. Attempting to find any progressive mp4 stream.")
            stream = yt.streams.filter(progressive=True, file_extension='mp4').first()

        if stream:
            download_file_path = os.path.join(download_path, filename) if filename else download_path
            print(f"Downloading video from: {url}")
            stream.download(output_path=download_path, filename=filename)  # Download the video to the download_path
            print("Download completed!")

            # Log download completion
            with open(log_file, 'a') as log:
                log.write(f"Downloaded: {yt.title} ({url}) to {download_file_path}\n")
        else:
            print("No suitable stream found with the specified criteria.")
            # Log the error
            with open(log_file, 'a') as log:
                log.write(f"Failed to download: {url}. No suitable stream found.\n")

    except VideoUnavailable:
        print(f"Video {url} is unavailable or age-restricted.")
        with open(log_file, 'a') as log:
            log.write(f"Video {url} is unavailable or age-restricted.\n")
    except Exception as e:
        print(f"An error occurred: {e}")
        # Log the error
        with open(log_file, 'a') as log:
            log.write(f"Error downloading {url}: {e}\n")

def download_show(url):
    download_path = "/mnt/omv/media/tvshows/"
    yt = YouTube(url, use_oauth=False, allow_oauth_cache=True)  # Create a YouTube object
    
    # Get all streams with progressive=True and file extension mp4
    streams = yt.streams.filter(progressive=True, file_extension='mp4')
    
    # Attempt to get the stream with 720p resolution
    stream_720p = streams.filter(resolution='720p').first()
    
    # If 720p stream is not found, get the highest resolution available
    if stream_720p is None:
        stream = streams.order_by('resolution').desc().first()
    else:
        stream = stream_720p
    
    print(f"Downloading video from: {url}")
    print(yt.title, yt.description, sep="\n")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Download a video from youtube using pytube')
    parser.add_argument('-mv', nargs=1, help='URL of the video to download')
    parser.add_argument('-M', nargs=1, help='URL of the music to download')
    parser.add_argument('-m', nargs=1, help='Download movies')
    parser.add_argument('-tv', nargs=1, help='Download tv shows')
    args = parser.parse_args()

    if args.mv:
        url = args.mv[0]
        download_video(url)
    elif args.M:
        url = args.M[0]
        download_music(url)
    elif args.m:
        url = args.m[0]
        download_movie(url)
    elif args.tv:
        url = args.tv[0]
        download_show(url)
    else:
        parser.print_help()


