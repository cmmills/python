#! /usr/bin/env python3

import workstation
import buttercup


def main():
    print("Starting workstation setup")
    workstation.do_all()
    buttercup.install_buttercup()

if __name__ == "__main__":
    main()