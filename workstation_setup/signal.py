#! /usr/bin/env python3

"""
Install signal on linux desktop
NOTE: These instructions only work for 64-bit Debian-based
Linux distributions such as Ubuntu, Mint etc.
"""
import subprocess
import os

def install_signal():
    # 1. Install our official public software signing key:
    subprocess.run(["wget", "-O-", "https://updates.signal.org/desktop/apt/keys.asc", "|", "gpg", "--dearmor", ">", "signal-desktop-keyring.gpg"])
    
    subprocess.run(["cat", "signal-desktop-keyring.gpg", "|", "sudo", "tee", "/usr/share/keyrings/signal-desktop-keyring.gpg", ">", "/dev/null"])
    
    # 2. Add our repository to your list of repositories:
    subprocess.run(["echo", "deb [arch=amd64 signed-by=/usr/share/keyrings/signal-desktop-keyring.gpg] https://updates.signal.org/desktop/apt lunar main", "|", "sudo", "tee", "-a", "/etc/apt/sources.list.d/signal-lunar.list"])
    
    
    # 3. Update your package database and install Signal:
    subprocess.run(["sudo", "apt", "update", "&&", "sudo", "apt", "install", "signal-desktop"])


if __name__ == "__main__":
    install_signal()