#! /usr/bin/evn python3

import os
import shutil
import subprocess

""" Backup script for backing up files to an external drive """

backup_media = "/media/" + os.environ['USER'] + "/B6F1-F592/"
backup_dir = backup_media + "backup"
user_home = os.path.expanduser("~")
ssh_dir = user_home + "/" + ".ssh"
env_file =  user_home + "/" + ".env"
host_file = "/etc/hosts"

def backup():
    """ Backup files to external drive """
    print("Backing up files to " + backup_dir)
    # check if backup directory exists
    if not os.path.exists(backup_dir):
        print("Backup directory not found")
        print("Creating backup directory")
        os.mkdir(backup_dir)
        
    # copy .env file to backup directory
    shutil.copy(env_file, backup_dir)
    # copy .ssh directory to backup directory
    subprocess.run(["cp", "-r", ssh_dir, backup_dir])
    # copy hosts file to backup directory
    subprocess.run(["sudo", "cp", host_file, backup_dir])

    print("Backup complete")

if __name__ == "__main__":
    # Check if external drive is mounted
    if os.path.exists(backup_media):
        backup()
    else:
        print("External drive " + backup_media +  " not found")