#! /usr/bin/python3

import requests
import os
import shutil
import subprocess

ver = "v0.5.13"
file_name = "lutris_0.5.13_all.deb"

def get_app():

    user = os.getenv("USER")
    os.chdir("/home/"+ user + "/")
    link = "https://github.com/lutris/lutris/releases/download/" + ver + "/" + file_name
    print("Downloading " + file_name + " from link "  + link)
    r = requests.get(link, allow_redirects=True)
    open('lutris.deb', 'wb').write(r.content)
    if r.status_code == 200:
        print("Downloaded " + file_name + " successfully")
    else:
        print("Download failed")

def install_app():
    user = os.getenv("USER")
    os.chdir("/home/"+ user + "/")
    print("Installing lutris" + file_name + "")
    subprocess.run(["sudo", "dpkg", "-i", "lutris.deb"])
    print("Removing lutris.deb")
    os.remove("lutris.deb")
    print("Installation completed")


get_app()
install_app()