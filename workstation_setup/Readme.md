# Workstation Setup

Installs development applications and creature comfort tools

- Install Buttercup password manager
- Install tools such as
    - git
    - nala
    - tree
    - btop
    - ncdu
    - cheat
    - tldr
    - neofetch