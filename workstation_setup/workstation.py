#! /usr/bin/env python3

import os
import shutil
import time
import subprocess
import argparse
import socket


def clear():
    os.system('clear')

def install_dev_tools():
    # Install Development Tools
    dev_list = [ "sudo apt-get -y update",
        "sudo apt-get -y install tree",
        "sudo apt-get -y install golang",
        "sudo apt-get -y install vlc",
        "sudo apt-get -y install tmux",
        "sudo apt-get -y install timeshift",
        "sudo apt-get -y install htop",
        "sudo apt-get -y install btop",
        "sudo apt-get -y install ncdu",
        "sudo apt-get -y install lsb-release",
        "sudo apt-get -y install curl",
        "sudo apt-get -y install git",
        "sudo apt-get -y install tldr",
        "sudo apt-get -y install neofetch",
        "sudo apt-get -y install nala",
    ]
    # 
    print("Installing development tools")
    time.sleep(2)
    for item in dev_list:
        os.system(item)
    print("Development tools installed")


def setup_git_config():
    clear()
    print("Setting up Git configuratin file")
    time.sleep(2)
    # 
    # Setup gitconfig
    os.system('git config --global user.name "Clarence Mills"')
    os.system('git config --global user.email "cmmills@protonmail.com"')
    os.system('git config --global core.editor "code --wait"')
    os.system('git config --global init.defaultBranch main')
    os.system('git config --global core.excludesfile ~/.gitignore_global')
    os.system('git config --global help.autocorrect 1')
    print("Git configuration file setup complete")

def setup_ssh():
    clear()
    print("Setting up SSH")
    time.sleep(2)
    # Setup SSH
    username = os.environ['USER']
    bits = int("4096")
    keytype = "rsa"
    comment = username + "@" + socket.gethostname()
    ssh_dir = os.path.join(os.environ['HOME'], ".ssh")
    os.system('ssh-keygen -t keytype -C comment -P "" -f  ssh_dir')

def install_cheat_sh():
    clear()
    print("setting up cheat.sh")
    time.sleep(2)
    PATH_DIR = os.path.expanduser("~")
    bin_dir = os.path.join(PATH_DIR, "bin")
    
    if not os.path.exists(bin_dir):
        os.makedirs(bin_dir)
    os.system('curl -s https://cht.sh/:cht.sh > ~/bin/cht.sh')
    os.system('chmod +x ~/bin/cht.sh')
    os.system("alias cheat='~/bin/cht.sh' >> ~/.bashrc")
    print("E.g. cheat python os")

def create_sudo_file():
    clear()
    print("Setting up sudo for user " + os.environ['USER'])
    time.sleep(2)
    file = os.environ['HOME'] + "/" + os.environ['USER']
    print("Creating file " + file + "in " + os.environ['HOME'])
    with open(file, 'w') as f:
        print(os.environ['USER'] + " ALL=(ALL) NOPASSWD:ALL", file=f)

def do_all():
    install_dev_tools()
    setup_git_config()
    install_cheat_sh()
    create_sudo_file()
    setup_ssh()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Workstation setup script')
    parser.add_argument('--all', action='store_true', help='Install all tools')
    parser.add_argument('--dev', action='store_true', help='Install development tools')
    parser.add_argument('--git', action='store_true', help='Setup gitconfig')
    parser.add_argument('--cheat', action='store_true', help='Setup cheat.sh')
    parser.add_argument('--sudo', action='store_true', help='Setup sudo for user')
    parser.add_argument('--ssh', action='store_true', help='Setup ssh')
    args = parser.parse_args()

    if args.all:
        clear()
        do_all()
        exit()
    if args.dev:
        install_dev_tools()
        exit()
    if args.git:
        setup_git_config()
        exit()
    if args.cheat:
        install_cheat_sh()
        exit()
    if args.sudo:
        create_sudo_file()
        exit()
    if args.ssh:
        setup_ssh()
        exit()
    else:
        parser.print_help()
