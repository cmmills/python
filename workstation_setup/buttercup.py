#! /usr/bin/env python3
import os
import shutil
import platform
import time

"""
Buttercup Password Manager Installer, this script will download and  install Buttercup Password Manager
"""

# Variables
app_dir = "/usr/local/bin"
app_name = "Buttercup-" + str.lower(platform.system()) + "-" + str(platform.machine()) + ".AppImage"
ver = "v2.20.3"
bcuplink = "https://github.com/buttercup/buttercup-desktop/releases/download/" + ver + "/" + app_name
download_dir = "/home/" + os.getlogin() + "/Downloads"

def install_buttercup():
    os.system("clear")
    print("Installing Buttercup Password Manager")
    print("Installing Buttercup dependencies")
    os.system("sudo add-apt-repository universe -y")
    os.system("sudo apt update -y")
    os.system("sudo apt install libfuse2 -y")

    print("Downloading Buttercup")
    time.sleep(2)
    os.chdir(download_dir)
    print("Downloading " + app_name + " from " + bcuplink)
    os.system("curl -s --fail --remote-name --location --continue-at - " + bcuplink)
   
    # Start Application
    print("Making " + app_name + " executable")
    os.system("sudo chmod +x " +  app_name)
    print("Moving " + app_name + " to " + app_dir)
    os.system("sudo mv " + app_name + " " + app_dir)
    print("Starting Butercup")
    print("Launching " + app_name)
    os.system(app_dir + "/" + app_name + " &")

if __name__ == "__main__":
    if platform.system() == "Linux":
        print("Linux OS detected")
        if os.path.exists(app_dir + "/" + app_name):
            print(app_name + " is already installed, exiting")
            exit(1)
        else:
            install_buttercup()
    else:
        print("Non Linux OS detected, exiting")
        exit(1)
        