#! /usr/bin/env bash

# requirements: pyinstaller https://pypi.org/project/pyinstaller/

# Clean up the build directories
cleanup(){
    if [[ -f "proxmox.spec"  && -d "build" ]]; then
        rm proxmox.spec
        rm -rf build dist
    fi
}

cleanup
# This script is used to build the project.
pyinstaller --onefile proxmox.py --name proxmox

# Create executable folder and move the proxmox binary to it.
if [[ -d "executable" ]]; then
    rm -rf executable
    mkdir executable
    mv dist/proxmox executable/
    cleanup
fi
