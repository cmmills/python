#!/usr/bin/python3

import paramiko
import argparse
import rich

# Proxmox hosts
hosts = "pve-1,pve-2,pve-3,pve-4"

def remote_command(server_name, command):
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    config = paramiko.SSHConfig()
    config.parse(open('/home/cmills/.ssh/config'))
    client.load_host_keys('/home/cmills/.ssh/known_hosts')
    client.connect(server_name, username=config.lookup(server_name)['user'],compress=True)
    stdin, stdout, stderr = client.exec_command(command)
    for line in stdout:
        rich.print(stdout.read().decode('utf-8'))
    if stdin.channel.recv_exit_status() != 0:
        for line in stderr:
            rich.print(stderr.read().decode('utf-8'))
    client.close()

def containers():
    rmt_command = 'qm list --full true'
    for host in hosts.split(','):
        print(host)
        remote_command(host,rmt_command )

def virtual_machines():
    rmt_comand = 'pct list'
    for host in hosts.split(','):
        print(host)
        remote_command(host,rmt_comand)

def all_servers():
    print("Listing virtual machines")
    rmt_command = 'pct list'
    for host in hosts.split(','):
        print(host)
        remote_command(host,rmt_command)
    
    print("Listing containers")
    rmt_command = 'qm list'
    for host in hosts.split(','):
        print(host)
        remote_command(host,rmt_command)

def cluster_info():
    rmt_command = 'pvecm status'
    remote_command(hosts.split(',')[0],rmt_command)

def cluster_nodes():
    rmt_command = 'pvecm nodes'
    remote_command(hosts.split(',')[0],rmt_command)

def get_next_free_vm_id():
    rmt_command = 'pvesh get /cluster/nextid'
    remote_command(hosts.split(',')[0],rmt_command)

def main():
    parser = argparse.ArgumentParser(description="Show Status of containers and Virtual servers on Proxmox nodes",epilog="Example: proxmox.py -v")
    parser.add_argument("-v", help="Show virtual machines", action="store_true")
    parser.add_argument("-c", help="show paravirtual containers", action="store_true")
    parser.add_argument("-a", help="show all servers", action="store_true")
    parser.add_argument("-C", help="show cluster info", action="store_true")
    parser.add_argument("-n", help="show cluster nodes", action="store_true")
    parser.add_argument("-f", help="show next free vm id", action="store_true")
    args = parser.parse_args()

    if args.v:
        virtual_machines()
    elif args.c:
        containers()
    elif args.a:
        all_servers()
    elif args.C:
        cluster_info()
    elif args.n:
        cluster_nodes()
    elif args.f:
        get_next_free_vm_id()
    else:
        parser.print_help()

if __name__ == "__main__":
    main()